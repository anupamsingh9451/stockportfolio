import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import Login from './components/login/index';
import Dashboard from './components/home/index';
import Sell from './components/sell/index';
import Buy from './components/buy/index';
import Portfolio from './components/portfolio/index';
class App extends React.Component{
	
    loginUser = localStorage.getItem('loginUser');
	render() {
		return (
		  <BrowserRouter>
			  <React.Suspense >
				<Routes>
					<Route path="/" element={<Login />} />
					<Route path="/login" element={<Login />} />
					<Route path="/home" element={<Dashboard />} />
					<Route exact path="/sell/:id" element={<Sell />} />
					<Route exact path="/buy/:id" element={<Buy />} />
					<Route exact path="/portfolio" element={<Portfolio />} />
				</Routes>
			  </React.Suspense>
		  </BrowserRouter>
		);
	}
}

export default App;

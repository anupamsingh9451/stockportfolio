import React,{useState,useEffect}  from 'react';
import loginService from '../../services/login';
import { useNavigate } from "react-router-dom";
import {Button,Row,Card,Container,Form} from 'react-bootstrap';

const Login= (props) =>{
    let navigate = useNavigate();

    useEffect(()=>{
        loginService
        .isLogin()
        .then((response) => {
            if(response){
                navigate("/home");
            }
        })
	},[])

    const [formData,setFormData] = useState({
        email:"",
        password:"",
    });

    const handleChange = (e) =>{
        const {name,value} = e.target
        setFormData({...formData,[name]:value})
    }

    const handleSubmit = (e) =>{
        e.preventDefault()
        loginService
        .login(formData)
        .then((response) => {
            if(response.data.code===200){
                navigate("/home");
            }else{
                alert(response.data.message);
            }
        })
        .catch((err) => {
            alert(err);
            console.log(err);
        });
    }

    return(
        <div className="container-fluid">
            <Row className=" cu-vertical-center">
                <div className="offset-lg-4 col-lg-4">
                    <Card>
                        <Card.Body className="card-body">
                            <Card.Title className='display-4 text-center'>
                                Stock Portfolio
                            </Card.Title>
                            <Container className="container">
                                <Form onSubmit={(event)=>{handleSubmit(event)}}>
                                    <Form.Group className="form-group">
                                        <label htmlFor="email">Email :</label>
                                        <input 
                                            type='email' 
                                            name='email' 
                                            placeholder='Enter email...' 
                                            required 
                                            onChange={(event)=>{handleChange(event)}}
                                            className="form-control"
                                        />
                                    </Form.Group>
                                    <Form.Group className="form-group">
                                        <label htmlFor="email">Password : </label>
                                        <input 
                                            type='password' 
                                            name='password' 
                                            placeholder='Enter password...'
                                            className="form-control"
                                            required 
                                            onChange={(event)=>{handleChange(event)}}
                                        />
                                    </Form.Group>                                    
                                    <Button
                                        type="submit"
                                        variant="primary"
                                        size="sm" 
                                        className="mt-2"
                                    >Log In</Button>
                                </Form>
                            </Container>
                        </Card.Body>
                    </Card>
                </div>
            </Row>
        </div>
    )
}

export default Login;
import React,{useState,useEffect} from 'react';
import {StockDataSelect} from './stockHtml';
import moment from "moment";

import {
    OverlayTrigger,
    Tooltip
} from 'react-bootstrap';

const Filter= (props) =>{
    const priceData = props.priceData
    const volumeData = props.volumeData;
    const [filterData,setFilterData] =useState({
        stock:"",
        fromDate:"",
        toData:"",
    });

    useEffect(() => {
        let tempPriceData = priceData;
        let tempVolumeData = volumeData;
        if(filterData?.fromDate){
            tempPriceData = tempPriceData.filter(function(elem) {
                let date1 = moment(filterData?.fromDate);
                let date2 = moment(elem?.time).subtract(1, 'M');
                return (date1.diff(date2, 'days')<=0)
            });
            tempVolumeData = tempVolumeData.filter(function(elem) {
                let date1 = moment(filterData?.fromDate);
                let date2 = moment(elem?.time);
                return (date1.diff(date2, 'days')<=0)
            });
        }
        if(filterData?.toData){
            tempPriceData = tempPriceData.filter(function(elem) {
                let date1 = moment(filterData?.toData);
                let date2 = moment(elem?.time).subtract(1, 'M');
                return (date1.diff(date2, 'days')>=0)
            });
            tempVolumeData = tempVolumeData.filter(function(elem) {
                let date1 = moment(filterData?.toData);
                let date2 = moment(elem?.time).subtract(1, 'M');;
                return (date1.diff(date2, 'days')>=0)
            });
        }
        
        props.setPriceData(tempPriceData);
        props.setVolumeData(tempVolumeData);
    },[filterData])

    const onChange = (name,value,text="")=>{
        if(filterData.fromDate!=="" && filterData.toDate!==""){
            let date1 = moment(filterData?.fromDate);
            let date2 = moment(filterData?.toData);
            if(name==='fromDate'){
                date1 = moment(value);
            }else if(name==='toData'){
                date2 = moment(value);
            }
            
            if(date2.diff(date1, 'days')<0){
                if(name==='fromDate'){
                    return alert('From Date must be less than To date');
                }else if(name==='toData'){
                    return alert('To Date must be greator than From date');
                }
            }else{
                setFilterData({...filterData,[name]:value});
            }
        }else{
            setFilterData({...filterData,[name]:value});
        }
		
	}
    

    return(
        <div className="row pb-2">
            <div className={(!props?.isStockFilter)?'d-none':'col-md-4' }>
                <StockDataSelect 
                    className="form-control-sm"
                    value={filterData.stock}
                    name="stock"
                    onChange={onChange}
                    isToolTip={true}
                    placeholder="Stocks"
                />
            </div>
            <div className="col-sm-4 ">
                <OverlayTrigger overlay={<Tooltip>From Date</Tooltip>}>
                    <input
                        type="date"
                        className="form-control form-control-sm"
                        value={filterData.fromDate}
                        name="fromDate"
                        onChange={(event)=>{onChange("fromDate",event.target.value) }}
                        placeholder="From Date"
                    />
                </OverlayTrigger>
            </div>
            <div className="col-sm-4">
                <OverlayTrigger overlay={<Tooltip>To Date</Tooltip>}>
                    <input
                        type="date"
                        className="form-control form-control-sm"
                        value={filterData.toData}
                        name="toData"
                        onChange={(event)=>{onChange("toData",event.target.value) }}
                        placeholder="To Date"
                    />
                </OverlayTrigger>
            </div>
        </div>
    );
}

export default Filter;
import React, { useEffect, useRef ,useState} from 'react';
import { createChart, CrosshairMode } from 'lightweight-charts';
import {priceData} from '../../services/priceData';
import {volumeData} from '../../services/volumeData';
import Filter from './filter';
import {
    Table,
} from 'react-bootstrap';


const TradingStockChart= (props) =>{
    const [priceData2,setPriceData] =useState(priceData);
    const [volumeData2,setVolumeData] =useState(volumeData);
    const [currentStock,setCurrentStock] =useState("");
    const chartContainerRef = useRef();
    const chart = useRef();
    const resizeObserver = useRef();
    
    useEffect(() => {
        getChartData();
    }, [priceData2]);
  
    // Resize chart on container resizes.
    useEffect(() => {
        resizeObserver.current = new ResizeObserver(entries => {
            const { width, height } = entries[0].contentRect;
            chart.current.applyOptions({ width, height });
            setTimeout(() => {
                chart.current.timeScale().fitContent();
            }, 0);
        });
  
        resizeObserver.current.observe(chartContainerRef.current);
  
        return () => resizeObserver.current.disconnect();
    }, [priceData2]);
  
    function getChartData(){
        const elements = document.getElementsByClassName("tv-lightweight-charts");
        while (elements.length > 0) elements[0].remove();

        chart.current = createChart(chartContainerRef.current, {
            width: chartContainerRef.current.clientWidth,
            height: 350,//(chartContainerRef.current.clientHeight)?chartContainerRef.current.clientHeight:350,
            layout: {
                backgroundColor: '#253248',
                textColor: 'rgba(255, 255, 255, 0.9)',
            },
            grid: {
                vertLines: {
                    color: '#334158',
                },
                horzLines: {
                    color: '#334158',
                },
            },
            crosshair: {
                mode: CrosshairMode.Normal,
            },
            priceScale: {
                borderColor: '#485c7b',
            },
            timeScale: {
                borderColor: '#485c7b',
            },
        });

        let candleSeries = chart.current.addCandlestickSeries({
            upColor: '#4bffb5',
            downColor: '#ff4976',
            borderDownColor: '#ff4976',
            borderUpColor: '#4bffb5',
            wickDownColor: '#838ca1',
            wickUpColor: '#838ca1',
        });
  
        candleSeries.setData(priceData2);
  
        let volumeSeries = chart.current.addHistogramSeries({
            color: '#182233',
            lineWidth: 2,
            priceFormat: {
            type: 'volume',
            },
            overlay: true,
            scaleMargins: {
            top: 0.8,
            bottom: 0,
            },
        });
  
        volumeSeries.setData(volumeData2);
    }

    return(
        <div>
            <Filter 
                isStockFilter={props?.isStockFilter} 
                priceData={priceData} 
                volumeData={volumeData} 
                setPriceData={setPriceData} 
                setVolumeData={setVolumeData}
            />
            <div ref={chartContainerRef} className="chart-container" />
        </div>
    );
}

export default TradingStockChart;
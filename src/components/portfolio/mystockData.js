import React from 'react';
import { Link } from 'react-router-dom';
import {stocksData} from '../../services/stocksData';
import {
    Badge,
    Button
} from 'react-bootstrap';

const MyStockDataTd= (props) =>{
    const stock = props.stock;
    const currentChange = stocksData.find(st=>st.id === stock.stock_id)
    
    const priceChange = (price,currentPrice) => {
        price =parseFloat(price) ?  parseFloat(price):0;
        currentPrice =parseFloat(currentPrice) ?  parseFloat(currentPrice):0;
        return (currentPrice-price)?.toFixed(2);
    }
    return(
        <tr>
            <td className="text-nowrap">
                <img src={stock.original_data?.logo_url} className="img-thumbnail rounded-circle border-0" width={30} height={30} alt={stock?.name}/>
                &emsp;
                {stock.original_data?.name}
            </td>
            <td>
                {stock?.quantity}
            </td>
            <td>
                {currentChange?.price}
            </td>
            <td className="btn-group">
                <Link to={"/buy/"+stock?.stock_id} size="sm" className="btn btn-sm btn-outline-primary py-0">Buy</Link>
                <Link to={"/sell/"+stock?.stock_id} size="sm" className="btn btn-sm btn-outline-danger py-0">Sell</Link>
            </td>
        </tr>
    );
}

export default MyStockDataTd;
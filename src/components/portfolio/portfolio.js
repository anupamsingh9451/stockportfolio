import React,{useState,useEffect} from 'react';
import {
    Table,
} from 'react-bootstrap';
import stockTransaction from '../../services/stockTransaction';
import MyStockDataTd from './mystockData';

const UserPortfolio= (props) =>{
    const [myStocks,SetMyStocks]= useState([]);

    useEffect(()=>{
        getMyStocks()
    },[])

    const getMyStocks = () =>{
        stockTransaction
        .getStocks()
        .then((response) => {
            if(response.data.code===200){
                SetMyStocks(response.data.data)
            }
        }).catch((err) => {
            alert("Something went wrong");
        });
    }

    return(
        <div>
            <Table striped  hover size="sm" variant="light" className="m-0">
                <thead>
                    <tr>
                        <th>Index</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Buy/Sell</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        myStocks?.map((stock,index)=>{
                            return (
                                <MyStockDataTd stock={stock} key={index}/>
                            );
                        })
                    }
                </tbody>
            </Table>
        </div>
    );
}

export default UserPortfolio;
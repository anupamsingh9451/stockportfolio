import React from 'react';
import { Link } from 'react-router-dom';
import {
    Badge,
    Table,
    Container
} from 'react-bootstrap';

const StockDetails= (props) =>{
    const stock = props.stock;
    const stockInHold = (props?.stockInHold)?props?.stockInHold:0;
    return(
        <Container>
            <Table className="mt-5 " width="100%">
                <tr>
                    <th scope="row">
                        Logo
                    </th>
                    <td> 
                        <img src={stock?.logo_url}  width={50} height={50} alt={stock?.name}/>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Name
                    </th>
                    <td> 
                        {stock?.name}
                    </td>
                </tr>
                <tr>
                    <th>
                        Currency
                    </th>
                    <td> 
                        {stock?.currency}
                    </td>
                </tr>
                <tr>
                    <th>
                        Price
                    </th>
                    <td> 
                        {stock?.price}
                    </td>
                </tr>
                <tr>
                    <th>
                        Price Change
                    </th>
                    <td> 
                        {stock['1d']?.price_change}
                    </td>
                </tr>
                <tr>
                    <th>
                        Percentage Change
                    </th>
                    <td> 
                        {stock['1d']?.price_change_pct}
                    </td>
                </tr>
                <tr>
                    <th>
                        Volume
                    </th>
                    <td> 
                        {stock['1d']?.volume}
                    </td>
                </tr>
                <tr>
                    <th>
                        Stock In Hold
                    </th>
                    <td> 
                        {stockInHold}
                    </td>
                </tr>
            </Table>
        </Container>
    );
}

export default StockDetails;
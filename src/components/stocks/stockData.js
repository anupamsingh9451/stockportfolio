import React from 'react';
import { Link } from 'react-router-dom';
import {
    Badge,
    Button
} from 'react-bootstrap';

const StockDataTd= (props) =>{
    const stock = props.stock;
    return(
        <tr>
            <td>
                <img src={stock?.logo_url} className="img-thumbnail rounded-circle border-0" width={30} height={30} alt={stock?.name}/>
                &emsp;
                {stock?.name}
            </td>
            <td>
                {stock?.price}
            </td>
            <td>
                {stock['1d']?.price_change}
            </td>
            <td>
                <Badge bg={(stock['1d']?.price_change_pct>0)?'success':'danger'}>{stock['1d']?.price_change_pct}</Badge>
            </td>
            <td className="btn-group">
                <Link to={"/buy/"+stock?.id} size="sm" className="btn btn-sm btn-outline-primary py-0">Buy</Link>
                <Link to={"/sell/"+stock?.id} size="sm" className="btn btn-sm btn-outline-danger py-0">Sell</Link>
            </td>
        </tr>
    );
}

export default StockDataTd;
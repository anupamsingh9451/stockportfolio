import React,{useState} from 'react';
import stockTransaction from '../../services/stockTransaction';
import {
    Container,
    Button,
} from 'react-bootstrap';

function BuyForm(props) {
    const [formData,setFormData] = useState({
        stock_id:props?.stock?.id,
        quantity:0,
        price:0,
        original_price:0,
        original_data:props?.stock
    });

    const onSubmitHandler = (event) =>{
        event.preventDefault();
        if(formData?.quantity==="" || parseInt(formData?.quantity)<1){
            return alert("Stock Quantity must be greator than 0");
        }else{
            stockTransaction
            .buyStock(formData)
            .then((response) => {
                if(response.data.code===200){
                    alert("Stock Added in your portfolio.");
                    let tempFormData ={
                        'quantity':0,
                        'price':0,
                        'original_price':0,
                    };
                    setFormData({...formData,...tempFormData});
                    props?.getStockInHand();
                }else{
                    alert("Something went wrong");
                }
            }).catch((err) => {
                alert("Something went wrong");
            });
        }
    }

    const onChange = (name,value) =>{
        let tempFormData ={
            'quantity':value,
            'price':parseFloat(value*props?.stock?.price)?.toFixed(2),
            'original_price':props?.stock?.price,
        };
        setFormData({...formData,...tempFormData});
    }

	return (
		<Container className="text-center py-5">
            <form className="row" onSubmit={(e)=>{onSubmitHandler(e)}}>
                <div className="col">
                    <label>Quantity</label>
                    <input 
                        type="number" 
                        name="quantity" 
                        className="form-control form-control-sm" 
                        placeholder="Quantity"
                        min="0"
                        value={formData?.quantity}
                        onChange={(event)=>{onChange('quantity',event.target.value)}}
                    />
                </div>
                <div className="col">
                    <label>Price</label>
                    <input 
                        type="number" 
                        name="price" 
                        className="form-control form-control-sm" 
                        placeholder="Price"
                        value={formData?.price}
                        readOnly
                    />
                </div>
                <div className="col">
                    <Button type="submit" className="mt-3 btn-sm">Buy</Button>
                </div>
            </form>
        </Container>
	);
}

export default BuyForm;

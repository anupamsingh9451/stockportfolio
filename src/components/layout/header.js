import React,{useState,useEffect} from 'react'
import { 
    Container,
} from 'react-bootstrap';
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav';
import {PROJECT_NAME} from '../../common/constants';
import loginService from '../../services/login';
import { useNavigate,Link } from "react-router-dom";

const Header = (props) => {
	let navigate = useNavigate();
	const logout = ()=>{
		loginService
        .logout()
        .then((response) => {
            navigate("/login");
        })
		
	}

	useEffect(()=>{
        loginService
        .isLogin()
        .then((response) => {
            if(!response){
                navigate("/login");
            }
        })
	},[])

  	return (
    	<Navbar bg="light" expand="lg">
			<Container>
				<Link to="/home" className="navbar-brand">{PROJECT_NAME}</Link>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="me-auto"></Nav> 
					<Nav>
						<Link to="/home" className="nav-link">Home</Link>
						<Link to="/portfolio" className="nav-link">Portfolio</Link>
						<Link to="#logout" className="nav-link" onClick={(e)=>logout()}>Logout</Link>
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
  	)
}

export default Header

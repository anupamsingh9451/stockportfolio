const LOCAL_STORAGE_KEY="stockTrans";

async function getStocks(stock_id="") {
    let stocks = localStorage?.getItem(LOCAL_STORAGE_KEY);
    let retriveStocks;
    let status =200;
    if(typeof(stocks)=='undefined'){
        status=409;
    }else{
        retriveStocks = (stocks)?JSON?.parse(stocks):[];
        
        if(stock_id!==""){
            retriveStocks = retriveStocks.find(stock=>stock?.stock_id===stock_id);
        }
    }
    let response ={
        'data':retriveStocks,
        'message':(retriveStocks)?"Data Sended Successfully":"No Data Found",
        'code':status,
    };
    return {
        'data':response
    }
}

async function buyStock(fields) {
    let stocks = localStorage?.getItem(LOCAL_STORAGE_KEY);
    if(typeof(stocks)=='undefined'){
        let tem=[];
        tem.push(fields)
        localStorage.setItem(LOCAL_STORAGE_KEY,tem);
    }else{
        let retriveStocks = (stocks)?JSON?.parse(stocks):[];
        
        if(retriveStocks?.length===0){
            retriveStocks.push(fields)
        }else if(retriveStocks.find(stock=>stock?.stock_id===fields?.stock_id)){
            retriveStocks.map(stock=>{
                if(stock?.stock_id===fields?.stock_id){
                    stock.quantity=(parseFloat(stock?.quantity)+parseFloat(fields?.quantity)).toFixed(2);
                    stock.original_price = fields?.original_price;
                    stock.original_data = fields?.original_data;
                }
                return stock;
            });
        }else{
            retriveStocks.push(fields);
        }
        let temRetriveStocks = JSON.stringify(retriveStocks);
        localStorage.setItem(LOCAL_STORAGE_KEY,temRetriveStocks);
    }
    let response ={
        data:{fields},
        message:"Stock Buyed Successfully",
        code:200,
    };
    return {
        data:response
    }
}

async function sellStock(fields) {
    let stocks = localStorage?.getItem(LOCAL_STORAGE_KEY);
    let status =200;
    let message = "Sell Successfully";
    if(typeof(stocks)=='undefined'){
        status=409;
        message="No Item Found to Sell";
    }else{
        let retriveStocks = (stocks)?JSON?.parse(stocks):[];
        
        if(retriveStocks?.length===0){
            status=409;
            message="No Item Found to Sell";
        }else if(retriveStocks.find(stock=>stock?.stock_id===fields?.stock_id)){

            retriveStocks.map(stock=>{
                if(stock?.stock_id===fields?.stock_id){
                    stock.quantity=(parseFloat(stock?.quantity)-parseFloat(fields?.quantity)).toFixed(2);
                    stock.original_price = fields?.original_price;
                    if(parseInt(stock?.quantity)<0){
                        status=409;
                        message="Quantity is less than hold stocks";
                    }
                }
                return stock;
            });
        }else{
            status=409;
            message="No Item Found to Sell";
        }
        let temRetriveStocks = JSON.stringify(retriveStocks);
        localStorage.setItem(LOCAL_STORAGE_KEY,temRetriveStocks);
    }
    let response ={
        'data':{fields},
        'message':message,
        'code':status,
    };
    return {
        'data':response
    }
}

const stockTransaction = {
    buyStock,
    getStocks,
    sellStock,
};

export default stockTransaction;

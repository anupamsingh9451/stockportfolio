const LOCAL_STORAGE_KEY="login";
const User = {
    email:'admin123@gmail.com',
    password:'123456'
};
let response ={
    data:{},
    message:"",
    code:0,
};
async function login(fields) {
    if(User.email!=="admin123@gmail.com"){
        response.message="Invalid Credentials";
        response.code=409;
    }else if(User.password!=="123456"){
        response.message="Invalid Credentials";
        response.code=409;
    }else{
        response.code=200;
        response.message="Login Successfully";
        localStorage.setItem(LOCAL_STORAGE_KEY,JSON.stringify(fields));
    }
    return {
        data:response
    }
}

async function logout(fields) {
    localStorage.removeItem(LOCAL_STORAGE_KEY);
}

async function isLogin(fields) {
    if(localStorage.getItem(LOCAL_STORAGE_KEY)){
        return true;
    }else{
        return false;
    }
}

async function getLogin(fields) {
    return localStorage.getItem(LOCAL_STORAGE_KEY);
}

const loginService = {
    login,
    logout,
    isLogin,
    getLogin,
};

export default loginService;
